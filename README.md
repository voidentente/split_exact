# split_exact

Implements splitting at patterns into exactly `N` destructurable slices.

Until `Pattern` API is stabilized, this crate currently only supports
single character separators.

# Usage

```rs
let header = "GET / HTTP/1.1";
let [method, path, http] = header.split_exact(char::is_whitespace);
```

Also with variant that returns remainder slice.

# License

MIT OR Apache-2.0
